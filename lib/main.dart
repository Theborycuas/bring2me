import 'package:bring2me/LoginPage.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(new MaterialApp(
   home: PrincipalPage(),
  ));
}

class PrincipalPage extends StatelessWidget {
  const PrincipalPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Bring2Me"),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            FlatButton(
              child: Text("Login"),
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(
                  builder: (context)=> MyAppLoginPage()
                ));
              },
            )
          ],
        ),
      ),
    );
  }
}